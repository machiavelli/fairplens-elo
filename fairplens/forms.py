from django import forms
from .models import Ticket, TournamentMatch

class TicketCreateForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = '__all__'
        exclude = ['creator', 'verified', 'verified_by']
        labels = {
            'who_won': 'Check if Red Team won, leave unchecked if Blue Team won :',
        }

class TicketVerifyForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ['verified']

class AddTournamentMatchForm(forms.ModelForm):
    class Meta:
        model = TournamentMatch
        fields = '__all__'
        labels = {
            'who_won': 'Check if Red Team won, leave unchecked if Blue Team won :',
        }