from tokenize import Single
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.utils import timezone

# Messages displayed when an action is successful or unsuccessful
from django.contrib import messages

# Logical expressions in .filter() for ManyToMany fields
from django.db.models import Q

from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()

from .models import Ticket, CasualMatch, TournamentMatch, SingleEliminationTournament
from .forms import TicketCreateForm, TicketVerifyForm, AddTournamentMatchForm

# Row-level permissions
from guardian.shortcuts import assign_perm, remove_perm



class HomepageView(generic.ListView):
    template_name = 'fairplens/homepage.html'
    context_object_name = 'latest_casual_matches_list'

    def get_queryset(self):
        return CasualMatch.objects.order_by('-date')[:5]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['latest_single_elimination_tournaments_list'] = SingleEliminationTournament.objects.order_by('-start_date')[:5]
        return context



class UserDetailView(generic.ListView):
    model = User
    template_name = 'fairplens/user_detail.html'
    context_object_name = 'casual_matches_list'

    # Get all matches played on either side
    def get_queryset(self):
        if self.request.user.is_authenticated:
            return CasualMatch.objects.filter(Q(red_side=self.request.user) | Q(blue_side=self.request.user)).distinct().order_by('-date')
        else:
            pass



class TicketDetailView(generic.DetailView):
    model = Ticket
    template_name = 'fairplens/ticket_detail.html'

    # Override function to check if the current user has the right permission
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['has_verify_permission'] = self.request.user.has_perm('can_verify_ticket', context['ticket'])
        return context

class TicketIndexView(generic.ListView):
    model = Ticket
    template_name = 'fairplens/ticket_index.html'
    context_object_name = 'ticket_list'    

    def get_queryset(self):
        # Get all tickets that can be verified by the user
        verifiable = []
        not_verified = Ticket.objects.filter(verified=False)
        for ticket in not_verified :
            if self.request.user.has_perm('can_verify_ticket', ticket) :
                verifiable.append(ticket.id)
        return Ticket.objects.filter(pk__in=verifiable)

class TicketCreateFormView(generic.edit.FormView):
    form_class = TicketCreateForm
    template_name = 'fairplens/ticket_create_form.html'

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        success_url = '/fairplens/ticket/create/'

        if not request.user.is_authenticated :
            messages.error(request, "Please log in to create a ticket. Ticket not created.")
        else :
            # The form data has to be valid
            if form.is_valid():

                blue_side = form.cleaned_data['blue_side']
                red_side = form.cleaned_data['red_side']

                blue_score = form.cleaned_data['blue_score']
                red_score = form.cleaned_data['red_score']
                who_won = form.cleaned_data['who_won']

                # The winner must have the most points (who_won = True iff Red won, Blue otherwise)
                if ((not who_won) and (blue_score > red_score)) or (who_won and (blue_score < red_score)):
                    # The ticket creator must be a player of the match
                    if (request.user in blue_side) or (request.user in red_side):

                        elo_registered = form.cleaned_data['elo_registered']
                        date = form.cleaned_data['date']

                        # Create the ticket with cleaned data and save it in the database
                        ticket = Ticket(creator=request.user, elo_registered=elo_registered, who_won=who_won, blue_score=blue_score, red_score=red_score, date=date)
                        ticket.save()
                        ticket.blue_side.set(blue_side)
                        ticket.red_side.set(red_side)

                        # Assign ticket verify permissions to the players belonging to the opposite side of the creator
                        if request.user in blue_side:
                            for player in red_side:
                                assign_perm('fairplens.can_verify_ticket', player, ticket)
                        else:
                            for player in blue_side:
                                assign_perm('fairplens.can_verify_ticket', player, ticket)

                        messages.success(request, "Ticket created ! The other team can now verify it.")
                        return HttpResponseRedirect(success_url)
                    else:
                        messages.error(request, "Only the players that took part in a match can create its ticket. Ticket not created.")
                else:
                    messages.error(request, "The winning team must have stricly more points than the other. Ticket not created.")

        return render(request, self.template_name, {'form': form})

class TicketVerifyFormView(generic.edit.FormView):
    form_class = TicketVerifyForm
    template_name = 'fairplens/ticket_verify_form.html'

    def get(self, request, pk):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, pk):
        form = self.form_class(request.POST)
        success_url = '/fairplens/'
        fail_url = '/fairplens/ticket/' + str(pk) + '/'

        if form.is_valid():
            ticket = Ticket.objects.get(pk=pk)

            # Ticket already verified
            if ticket.verified:
                messages.error(request, "This ticket has already been verified.")
                return HttpResponseRedirect(fail_url)

            else:
                if request.user.has_perm('can_verify_ticket', ticket):
                    verified = form.cleaned_data['verified']
                    if verified:
                        # Successful verification
                        ticket.verified = verified
                        ticket.verified_by = request.user
                        ticket.save()

                        match = CasualMatch(elo_registered=ticket.elo_registered, who_won=ticket.who_won, blue_score=ticket.blue_score, red_score=ticket.red_score, date=ticket.date, origin_ticket=ticket)
                        match.save()
                        match.blue_side.set(ticket.blue_side.all())
                        match.red_side.set(ticket.red_side.all())

                        # Remove ticket verify permissions from the opposite side of the creator
                        if request.user in ticket.blue_side.all():
                            for player in ticket.blue_side.all():
                                remove_perm('fairplens.can_verify_ticket', player, ticket)
                        else:
                            for player in ticket.red_side.all():
                                remove_perm('fairplens.can_verify_ticket', player, ticket)

                        messages.success(request, "Ticket verified ! The corresponding match has been created.")
                        return HttpResponseRedirect(success_url)
                # User doesn't have the right permission to validate the ticket
                else:
                    messages.error(request, "You don't have permission to verify this ticket. Ticket not verified.")
                    return HttpResponseRedirect(fail_url)

        return render(request, self.template_name, {'form': form})



class CasualMatchDetailView(generic.DetailView):
    model = CasualMatch
    context_object_name = 'match'
    template_name = 'fairplens/casual_match_detail.html'

class TournamentMatchDetailView(generic.DetailView):
    model = TournamentMatch
    context_object_name = 'match'
    template_name = 'fairplens/tournament_match_detail.html'

    # Get the previous matches
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_match = TournamentMatch.objects.get(pk=self.kwargs['pk'])
        next_match = None
        for match in TournamentMatch.objects.get(pk=self.kwargs['pk']).tournament.first().matches.all():
            if current_match in match.previous_matches.all():
                next_match = match
        context['previous_match_list'] = current_match.previous_matches.all()
        context['next_match'] = next_match
        return context



class SingleEliminationTournamentDetailView(generic.DetailView):
    model = SingleEliminationTournament
    context_object_name = 'tournament'
    template_name = 'fairplens/single_elimination_tournament_detail.html'

    # Get the list of matches
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tournament_match_list'] = SingleEliminationTournament.objects.get(pk=self.kwargs['pk']).matches.all()
        return context

class AddTournamentMatchFormView(generic.edit.FormView):
    form_class = AddTournamentMatchForm
    template_name = 'fairplens/add_tournament_match_form.html'

    def get(self, request, pk):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, pk):
        form = self.form_class(request.POST)
        success_url = '/fairplens/singleeliminationtournament/' + str(pk) + '/addmatch/'
        fail_url = '/fairplens/singleeliminationtournament/' + str(pk) + '/'

        tournament = SingleEliminationTournament.objects.get(pk=pk)

        # Check if it is not too late for the organisers to create matches
        if tournament.end_permission_date < timezone.now():
            messages.error(request, "The 'end_permission_date' has been reached. You can't add matches anymore.")
        else:
            if not request.user in tournament.organisers.all():
                messages.error(request, "You are not an organiser of this tournament : you can't add a match.")
            else:
                # The form data has to be valid
                if form.is_valid():

                    blue_score = form.cleaned_data['blue_score']
                    red_score = form.cleaned_data['red_score']
                    who_won = form.cleaned_data['who_won']

                    # The winner must have the most points (who_won = True iff Red won, Blue otherwise)
                    if ((not who_won) and (blue_score > red_score)) or (who_won and (blue_score < red_score)):
                            
                        name = form.cleaned_data['name']
                        date = form.cleaned_data['date']

                        blue_side = form.cleaned_data['blue_side']
                        red_side = form.cleaned_data['red_side']

                        previous_matches = form.cleaned_data['previous_matches']

                        # Create the match with cleaned data and save it in the database
                        match = TournamentMatch(name=name, who_won=who_won, blue_score=blue_score, red_score=red_score, date=date)
                        match.save()
                        match.blue_side.set(blue_side)
                        match.red_side.set(red_side)
                        match.previous_matches.set(previous_matches)

                        # Add match to tournament
                        tournament.matches.add(match)

                        messages.success(request, "Match created !")
                        return HttpResponseRedirect(success_url)
                    else:
                        messages.error(request, "The winning team must have stricly more points than the other. Match not created.")

        return render(request, self.template_name, {'form': form})