from django.urls import path

from . import views

app_name = 'fairplens'
urlpatterns = [
    # ex: /fairplens/
    path('', views.HomepageView.as_view(), name='homepage'),

    path('user/', views.UserDetailView.as_view(), name='user_detail'),

    path('ticket/', views.TicketIndexView.as_view(), name='ticket_index'),
    path('ticket/create/', views.TicketCreateFormView.as_view(), name='ticket_create_form'),
    # ex: /fairplens/ticket/3/verify
    path('ticket/<int:pk>/verify', views.TicketVerifyFormView.as_view(), name='ticket_verify_form'),
    # ex: /fairplens/ticket/5
    path('ticket/<int:pk>/', views.TicketDetailView.as_view(), name='ticket_detail'),

    # ex: /fairplens/casualmatch/12
    path('casualmatch/<int:pk>/', views.CasualMatchDetailView.as_view(), name='casual_match_detail'),

    # ex: /fairplens/tournamentmatch/53
    path('tournamentmatch/<int:pk>/', views.TournamentMatchDetailView.as_view(), name='tournament_match_detail'),

    # ex: /fairplens/singleeliminationtournament/5
    path('singleeliminationtournament/<int:pk>/', views.SingleEliminationTournamentDetailView.as_view(), name='single_elimination_tournament_detail'),
    path('singleeliminationtournament/<int:pk>/addmatch/', views.AddTournamentMatchFormView.as_view(), name='add_tournament_match_form'),
]