# Generated by Django 4.0.4 on 2022-05-18 21:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fairplens', '0002_alter_member_memberships'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='memberships',
            field=models.JSONField(blank=True, null=True),
        ),
    ]
