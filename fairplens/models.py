from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser

from datetime import datetime

"""
models.User :
- username
- first_name (optional, blank=True)
- last_name (optional, blank=True)
- email (optional, blank=True)
- password (hash and metadata)
x groups
- user_permissions (many-to-many with Permission)
- is_staff (can access admin site or not)
- is_active (better deactivate an account than delete it)
- is_superuser (all permissions without assigning them)
- last_login (datetime)
- date_joined
"""

class Member(AbstractUser):
    memberships = models.JSONField(null=True, blank=True)
    elo = models.IntegerField(default=0)



class Ticket(models.Model):
    class Meta:
        permissions = [('can_verify_ticket', 'Can verify ticket')]

    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='tickets_created')
    verified = models.BooleanField(default=False)
    verified_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.DO_NOTHING, related_name='verified_tickets')

    elo_registered = models.BooleanField(default=False)
    who_won = models.BooleanField(default=False) # False for blue, True for red
    blue_score = models.SmallIntegerField(default=0)
    red_score = models.SmallIntegerField(default=0)
    date = models.DateTimeField('Date', default=datetime.now)

    blue_side = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='tickets_played_blue_side')
    red_side = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='tickets_played_red_side')

    def __str__(self):

        pp_blue = 'B : '
        pp_red = 'R : '
        winner = 'Winner : Red' if self.who_won else 'Winner : Blue'

        for i in self.blue_side.all():
            pp_blue += i.username + ', '

        for i in self.red_side.all():
            pp_red += i.username + ', '

        return str(self.pk)+' | '+pp_blue[:-2]+' \\'+str(self.blue_score)+'/ | \\'+str(self.red_score)+'/ '+pp_red[:-2]+' | '+winner



class CasualMatch(models.Model):
    elo_registered = models.BooleanField(default=False)
    who_won = models.BooleanField(default=False)
    blue_score = models.SmallIntegerField(default=0)
    red_score = models.SmallIntegerField(default=0)
    date = models.DateTimeField('Date', default=datetime.now)

    blue_side = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='casual_matches_played_blue_side')
    red_side = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='casual_matches_played_red_side')

    origin_ticket = models.OneToOneField(Ticket, null=True, on_delete=models.CASCADE, related_name='related_match')

    def __str__(self):

        pp_blue = 'B : '
        pp_red = 'R : '
        winner = 'Winner : Red' if self.who_won else 'Winner : Blue'

        for i in self.blue_side.all():
            pp_blue += i.username + ', '

        for i in self.red_side.all():
            pp_red += i.username + ', '

        return str(self.pk)+' | '+pp_blue[:-2]+' \\'+str(self.blue_score)+'/ | \\'+str(self.red_score)+'/ '+pp_red[:-2]+' | '+winner



class TournamentMatch(models.Model):
    # Possibility to give a name to easily find a match in a given tournament
    name = models.CharField(blank=True, max_length=64)

    who_won = models.BooleanField(default=False)
    blue_score = models.SmallIntegerField(default=0)
    red_score = models.SmallIntegerField(default=0)
    date = models.DateTimeField('Date', default=datetime.now)

    blue_side = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='tournament_matches_played_blue_side')
    red_side = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='tournament_matches_played_red_side')

    # Used for the tournament matches tree
    previous_matches = models.ManyToManyField("self", blank=True, symmetrical=False)

    def __str__(self):

        if self.name == '':
            pp_blue = 'B : '
            pp_red = 'R : '
            winner = 'Winner : Red' if self.who_won else 'Winner : Blue'

            for i in self.blue_side.all():
                pp_blue += i.username + ', '

            for i in self.red_side.all():
                pp_red += i.username + ', '

            return str(self.pk)+' | '+pp_blue[:-2]+' \\'+str(self.blue_score)+'/ | \\'+str(self.red_score)+'/ '+pp_red[:-2]+' | '+winner

        else:
            return self.name

class SingleEliminationTournament(models.Model):
    name = models.CharField(blank=True, max_length=64)
    organisers = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='single_elimination_tournaments_organised')
    matches = models.ManyToManyField(TournamentMatch, blank=True, related_name='tournament')
    start_date = models.DateTimeField('Start date', default=datetime.now)
    end_date = models.DateTimeField('End date', default=datetime.now)

    # Date past which the organisers can't change the tournament and its matches anymore
    end_permission_date = models.DateTimeField('End of the permissions', default=datetime.now)

    def __str__(self):
        if self.name == '':
            return str(self.pk) + " | Single elimination tournament - [ " + str(self.start_date) + " ; " + str(self.end_date) + " ]"
        else:
            return str(self.pk) + " | " + self.name
