from django.apps import AppConfig


class FairplensConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fairplens'
